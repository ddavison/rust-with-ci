fn main() {
    println!("Add: 1 + 2 = {}", add(1, 2));
    println!("Subtract: 3 - 2 = {}", subtract(3, 2));
}

fn add(first: i32, second: i32) -> i32 {
    first + second
}

fn subtract(first: i32, second: i32) -> i32 {
    first - second
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_add() {
        assert_eq!(add(1, 2), 3);
    }

    #[test]
    fn test_subtract() {
        assert_eq!(subtract(3, 2), 1);
    }
}